
# _**中文教程**_ :tw-1f34b: 
# CJXYORM
如果你厌倦了繁琐的JDBC，Apache的DBUtils又满足不了你的需求,mybaits、hibernate却又太过于庞大，那么就请使用cjxyorm吧，它吸收了DBUtils和Hibernate的特点，简单易用没有任何多余功能。轻轻松松的进行CRUD。

# **入门demo** 
准备工作：在工程中添加全部的jar包

第一步：配置相关配置
把c3p0-config.xml、cjxy-orm.xml放到编译路径下
c3p0文件内容：
```
<?xml version="1.0" encoding="UTF-8"?>
<c3p0-config>
	<default-config>
		<property name="user">root</property>
		<property name="password">***********</property>
		<property name="driverClass">com.mysql.jdbc.Driver</property>
		<property name="jdbcUrl">
		  <![CDATA[jdbc:mysql:///cjxy?useUnicode=true&characterEncoding=utf-8]]>
		</property>
	</default-config> 
</c3p0-config> 
```

cjxy-orm.xml内容如下
```
<?xml version="1.0" encoding="UTF-8"?>
<cjxy-orm-config> 
    <session-factory>
        <!-- 只可以放在类路径下面  开启 c3p0-->
        <property name="orm.c3p0">true</property>
        <!--在控制台打印sql-->
        <property name="show_sql">true</property>
        <!-- mysql(5.5) oracle(10g) -->
        <property name="orm.dialect">mysql</property>
        <!-- 映射Pojo -->
        <mapping class="User"/>
    </session-factory>
</cjxy-orm-config>
```



第二步：新建一个DBUtils.java的工具类
```

import cn.cjxy.orm.main.QuerySession;
import cn.cjxy.orm.main.SessionFactory;

public class DBUtils{
    public static QuerySession getQuerySession(){
        return new SessionFactory().openQuerySession();
    }
}
```

第三步：新建一个实体类User.java
```
import cn.cjxy.orm.anntaion.Enitry;
import cn.cjxy.orm.anntaion.GeneratedValue;
import cn.cjxy.orm.anntaion.Id;
import cn.cjxy.orm.anntaion.Table;
import cn.cjxy.orm.constant.ORMConfig;

@Enitry
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(startegy = ORMConfig.IDENTITY)
    private Integer id;

    private String name;

    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
```

第四步：新建一个测试类Test.java

```
public class Test{
   
   @Test
   public void testSessionFactory(){
        User user=new User();
        user.setName("jiangfan");
        user.setPassword("123456");   
        DBUtils.getQuerySession().save(user);   
   }  
}
```

 ### 如果您的配置没有问题的话,您的数据库中会生成一张表和一条记录。 


# cjxyorm注解详解
### @Enitry
#### 这个注解用来修饰一个实体类,如果一个类被此注解修饰。则这个类会被cjxyorm识别。然后读取此类的的详细信息,然后生成一张表。如果您想要这个类就必须加上此注解

```
@Enitry
public class User
{
   
}
```
### @Table(name="table_name")
#### 这个注解同样也是用来修饰一个实体类，这个注解用来指定生成数据表的名字。这个注解并不是必须的，如果您不使用此注解，则默认会使用类的名字作为数据表的名字。
```
@Enitry
@Table(name= "my_user")
public class User
{
     
}
```
### @Id、 @GeneratedValue
#### @Id 注解用来修饰类当中的属性，这个注解是用来指定生成数据表的字段的主键，被修饰的属性必须是Integer且名字为id。否则cjxyorm将不能正常 运行。@Id注解还需要配合 @GeneratedValue注解才会起作用。@GeneratedValue注解里面有一个startegy属性，startegy用来指定主键的生成策略。
#### ORMConfig.IDENTITY指定策略为id自增。
```
@Enitry
@Table(name= "my_user")
public class User
{
    @Id
    @GeneratedValue(startegy = ORMConfig.IDENTITY) //抱歉目前cjxyorm只支持自增一种生成策略
    private  Integer id;
    
   // ..... setter/getter 
     
}
```
###  @Column
#### @Column注解用来修饰类中的属性。它里面有这些属性：length、name、nullable、unique、type length属性用来指定生成数据表字段的长度,name用来指定生成数据表字段的名字,nullable指定生成数据表字段是否为空(默认为空)，unique用来指定数据表字段是否是唯一的(默认是不唯一),type用来指定数据表字段的类型(默认为varchar(255))。此注解也不是必须的,如果您不使用此注解，那么没有被此注解修饰的属性将将会根据自己的名字生成一个字段且类型为varchar长度为255。
```
@Enitry
@Table(name= "my_user")
public class User
{
    @Id
    @GeneratedValue(startegy = ORMConfig.IDENTITY) //抱歉目前cjxyorm只支持自增一种生成策略
    private  Integer id;
    
    @Column(name="USER_NAME",type = "varchar",length = "50",unique = true,nullable = true)
    private  String username;
    
    private  String password;
    
    // ..... setter/getter 
        
}
```
# cjxyorm SessionFactory类详解
### SessionFactory
#### 创建SessionFactory的实例我们可以调用这个类中的 openSession()方法和openQuerySession()方法,这个两个方法分别返回 Session的实例和
#### QuerySession的实例,本人推荐大家使用QuerySession,因为它对Session进行了扩展。下面我们定义一个工具类来演示如何调用SessionFactory中的两个方法
```
public class DBUtils {
    private static SessionFactory sessionFactory=new SessionFactory();

    public  static Session getSession(){
        return sessionFactory.openSession();
    }

    public static QuerySession getQuerySession(){
        return sessionFactory.openQuerySession();
    }
}
```
### QuerySession
#### 我在QuerySession定义了一系类CRUD的方法,下面我将介绍一些比较常用的方法。
#### public <T> Object save(Object t)：这个方法需要一个传入一个Object类型的对象,cjxyorm会通过反射机制获取这个对象中set的属性值，然后拼接成一条sql语句，并持久化到数据库中。 

```
  public class Hello {
    public static void main(String[] args) {
        User user=new User();
        user.setUsername("张三");
        user.setPassword("123456");
        DBUtils.getSession().save(user);
    }
}
```
#### public <T> T get(Serializable id, Class<T> clazz)：这个方法需要传入两个参数，第一个是序列化id,第二个是带泛型Class对象，cjxyorm会先去寻找传入Class对象中的 @Table注解，如果这个注解存在就根据这个注解指定名字和传入的序列化id生成一条查询语句，并且将结果封装到Class对象中并返回。如果@Table注解不存在就根据类名和传入的序列化id来生成查询语句，然后和上面一样封装结果，并返回。
```
public class Hello {
    public static void main(String[] args) {
        System.out.println(DBUtils.getSession().get(1,User.class));//在user中查找id为1的记录。
    }
}
```
#### public <T> void delete(Serializable id, Class<T> clazz)：这个方法和上面的get方法用法一样，唯一不同的是上面的方法是从表中取一条指定记录，而这个方法是从表删除指定的一条的记录。

```
public class Hello {
    public static void main(String[] args) {
        DBUtils.getSession().delete(1,User.class);//在user中删除id为1的记录
    }
}
```
#### public void update(Serializable id, Object obj)：这个方法需要传入两个参数，第一个是序列化id,第二个是Object的实例，cjxyorm会通过反射机制获取传入obj对象中set的属性值，然后根据根据传入的序列化id 来生成一条UPDATE语句。

```
  public class Hello {
    public static void main(String[] args) {
        User user=new User();
        user.setUsername("姜小白");
        user.setPassword("1314520");
        BUtils.getSession().save(user);//插入一条记录
        System.out.println( BUtils.getSession().get(2,User.class));//查询刚才保存的记录
        
        user.setUsername("江小白");//修改username
        DBUtils.getSession().update(2,user);//执行修改
        System.out.println( BUtils.getSession().get(2,User.class));//在这里会显示你修改之后的结果
    }
}
```
#### <T> T queryOne(String sql, Class<T> type, Object[] parames):这个方法需要传入三个参数，第一个是您的查询sql,第二个是你要将查询的结果封装的类型，第三个是查询参数。

```
 public class Hello {
    public static void main(String[] args) {
        String sql="SELECT * FROM my_my_user WHERE USER_NAME=? AND password=?";
        Object[] objs=new Object[]{"江小白",1314520};
        User result=DBUtils.getQuerySession().queryOne(sql,User.class,objs);
        System.out.println(result);
    }
}
```
#### <T> Set<Object> queryList(String sql, Class<T> type, Object[] parames):这个方法和上面一个方法用法是一样的，只不过它查询的多条记录，我们需要注意的是queryList的返回值是 Set 集合并不是 List。
```
 public class Hello {
    public static void main(String[] args) {
        List list=new ArrayList();
        User obj=new User();
        for(int i=0;i<=99999;++i){
            obj.setUsername("江小白"+i);
            obj.setPassword("1314520"+i);
            list.add(obj);
        }
        DBUtils.getQuerySession().insertBatch(list);//批量插入

        String sql="SELECT * FROM my_my_user WHERE id>?";
        Object[] objs=new Object[]{1};
        Set result=DBUtils.getQuerySession().queryList(sql,User.class,objs);
        result.forEach(item-> System.out.println(item));//迭代结果集
    }
 }
```
#### 在上面的代码中我使用到 queryList方法 这是他的方法签名：<T> void insertBatch(List<Object> list) 他需要传入一个list对象，cjxyorm会遍历这个list，然后批量把list中的对象持久化到数据库中。 

####  public void delete(String sql, Object[] parames)这个方法需要两个参数,他的用法非常简单。
```
  public class Hello {
    public static void main(String[] args) {
        String sql="DELETE FROM my_user WHERE id=?";
        Object[] objs=new Object[]{2};
        DBUtils.getQuerySession().delete(sql,objs);//删除指定的记录
    }
  }
```
#### public void update(String sql, Object[] parames)这个方法和上面使用方法是一样的。
```
public class Hello {
    public static void main(String[] args) {
        String sql="UPDATE SET USER_NAME=? WHERE id=?";
        Object[] objs=new Object[]{"小白",2};
        DBUtils.getQuerySession().update(sql,objs);//按条件更改
    }
}
```

#### public <T> long size(Class<T> clazz)：这个方法需要传入一个Class类型的参数，cjxyorm会去寻找clazz对象中的@Table注解，通过注解中指定的名字来生成一条统计SQL,否则就根据类名来生成。
```
public class Hello {
    public static void main(String[] args) {
       int count=DBUtils.getQuerySession().size(User.class);
       System.out.println(count);
    }
}
```
#### public long size(String sql, Object[] parames):这个方法和上面的方法作用是一样的，唯一不同的是使用方式有略微的差别。
```
public class Hello {
    public static void main(String[] args) {
        String sql="SELECT COUNT(*) FROM my_user WHERE id>?";
        Object [] objs=new Object[]{500};
        int count=(int)DBUtils.getQuerySession().size(sql,objs);//按条件统计
        System.out.println(count);
    }
}
```

#### <T> Set<Object> pageNo(Class<T> clazz, int next):这个方法需要传入两个参数，第一个是Class类型的对象，第二个是参数页码。cjxyorm会去寻找clazz对象中的@Table注解，如果这个注解存在就根据注解中指定的名字和传入的页码生成一条分页语句，并把结果封装到Class类型的对象中，并返回。如果注解不存在就根据类名来生成。还有一点我们还可以通过setPageSize(number)去设置每一页的数量。
```
public class Hello {
    public static void main(String[] args) {
         DBUtils.getQuerySession().setPageSize(20);
         Set result=DBUtils.getQuerySession().pageNo(User.class,2);
         result.forEach(item-> System.out.println(item));
    }
}
```

------------------------------------------------------------------------------------------------------------------


# _**English Course**_ :tw-1f30b: 

# CJXYORM
If you are tired of the cumbersome JDBC, Apache DBUtils can not meet your needs, mybaits, hibernate is too large, so please use cjxyorm bar, it absorbs the characteristics of DBUtils and Hibernate, simple and easy to use, without any redundant function. Easy to carry out CRUD.
# Simple example
###  Preparation: add all jar packages to the project
### The first step: configure the relevant files, put c3p0-config.xml and cjxy-orm.xml into the compile path
#### C3p0 file content:
```
<?xml version="1.0" encoding="UTF-8"?>
<c3p0-config>
    <default-config>
        <property name="user">root</property>
        <property name="password">***********</property>
        <property name="driverClass">com.mysql.jdbc.Driver</property>
        <property name="jdbcUrl">
          <![CDATA[jdbc:mysql:///cjxy?useUnicode=true&characterEncoding=utf-8]]>
        </property>
    </default-config> 
</c3p0-config>
```

#### Cjxy-orm.xml content
```
<?xml version="1.0" encoding="UTF-8"?>
<cjxy-orm-config> 
    <session-factory>
      
        <property name="orm.c3p0">true</property>
      
        <property name="show_sql">true</property>
        <!-- mysql(5.5) oracle(10g) -->
        <property name="orm.dialect">mysql</property>
      
        <mapping class="User"/>
    </session-factory>
</cjxy-orm-config>
```
#### The second step: create a new tool class for DBUtils.java
```
import cn.cjxy.orm.main.QuerySession;
import cn.cjxy.orm.main.SessionFactory;

public class DBUtils{
    public static QuerySession getQuerySession(){
        return new SessionFactory().openQuerySession();
    }
}
```
#### The third step: create a new entity class User.java
```
import cn.cjxy.orm.anntaion.Enitry;
import cn.cjxy.orm.anntaion.GeneratedValue;
import cn.cjxy.orm.anntaion.Id;
import cn.cjxy.orm.anntaion.Table;
import cn.cjxy.orm.constant.ORMConfig;

@Enitry
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(startegy = ORMConfig.IDENTITY)
    private Integer id;

    private String name;

    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
```
#### The fourth step: create a new test class Test.java
```
public class Test{

   @Test
   public void testSessionFactory(){
        User user=new User();
        user.setName("jiangfan");
        user.setPassword("123456");   
        DBUtils.getQuerySession().save(user);   
   }  
}
```

#### All the work has been done here, and if your configuration has no problem, then a table will be created in the database to insert a record

------------------------------------------------------------------------------------------------------------------

### 您的支持就是我最大的动力(Your support is my biggest motivation) :tw-1f49b:  
 #### 支付宝(Alipay)
![支付宝](https://gitee.com/uploads/images/2017/1116/160722_0c461ddd_1636012.jpeg "1510819602224.jpg")
#### QQ红包(QQ red packets)
![qq红包](https://gitee.com/uploads/images/2017/1116/160455_c6c9a898_1636012.png "无标题.png")
#### 微信支付(WeChat payment)
![微信支付](https://gitee.com/uploads/images/2017/1116/160537_88de5f06_1636012.png "mm_facetoface_collect_qrcode_1510819196278.png")