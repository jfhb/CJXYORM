package cn.cjxy.orm.constant;

public class ORMConfig {
   public static final String IDENTITY="IDENTITY";
   public static final String AUTO="Auto";
   public static final String UUID="UUID";
   public static final String INT="int";
}