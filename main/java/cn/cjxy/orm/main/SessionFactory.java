package cn.cjxy.orm.main;

public class SessionFactory {
	
	public Session openSession(){
		return Session.getInstanceof();
	}
	
    public QuerySession openQuerySession(){
    	return QuerySession.getInstanceof();
    }
}