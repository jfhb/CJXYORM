package cn.cjxy.orm.main;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface QueryFactory{
	/**
	 * ��ѯһ��list
	 * @param sql ��ѯ���
	 * @param clazz ���������
	 * @param params ��ѯ����
	 * @return
	 */
	public <T>Set<Object> queryList(String sql, Class<T> clazz, Object[] params);
	
	/**
	 * ��ѯһ������
	 * @param sql ��ѯ���
	 * @param clazz ���������
	 * @param params ��ѯ����
	 * @return
	 */
    public <T> T queryOne(String sql, Class<T> clazz, Object[] params);
    
    /**
     * ɾ��
     * @param sql ɾ�����
     * @param params ����
     */
    public void delete(String sql, Object[] params);
    
    /**
     * ����
     * @param sql ɾ�����
     * @param params ����
     */
    public void update(String sql, Object[] params);
    
    /**
     * ����ɾ��
     * @param sql
     * @param params
     */
    public void deleteBatch(String sql, List<Object[]> list);
    
    /**
     * ��������
     * @param sql
     * @param params
     */
    public void updateBatch(String sql, List<Object[]> list);
    
    /**
     * ��������
     * @param sql
     * @param list
     */
    public <T>void insertBatch(List<Object> list);
    
    /**
     * �l���yӋ
     * @param sql
     * @param params
     * @return
     */
    public long size(String sql, Object[] params);
    
    /**
     * 
     * @param sql
     * @param clazz
     * @param params
     * @return
     */
    public <T>Set<Object> pageNo(String sql, Class<T> clazz, Object[] params, int next);
   
    /**
     * 
     * @param clazz
     * @param next
     * @return
     */
    public <T>Set<Object> pageNo(Class<T> clazz, int next);
}
